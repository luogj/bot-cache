import Redis from "redis";

let redis;

function init(redisUrl) {
  return new Promise((resolve) => {
    redis = Redis.createClient(redisUrl);
    redis.on("error", (err) => {
      console.error("Redis encountered an error!");
      console.error(err);
      console.error("");
    });
    redis.on("connect", () => {
      console.log("Connected to redis server!");
      console.log("");
      resolve();
    });
  });
}

function getFromRedis(key) {
  return new Promise((resolve, reject) => {
    redis.get(key, (err, reply) => {
      if (err) {
        reject();
      }
      resolve(reply);
    });
  });
}

function setToRedis(key, value) {
  return new Promise((resolve, reject) => {
    redis.set(key, value, (err, reply) => {
      if (!err) {
        resolve(reply);
      }
      reject();
    });
  });
}

function delFromRedis(key) {
  return new Promise((resolve, reject) => {
    redis.del(key, (err, reply) => {
      if (!err) {
        resolve(reply);
      }
      reject();
    });
  });
}

function getLatestKeyValuePairs() {
  let latest = {};
  return new Promise((resolve) => {
    redis.keys("*", (err, reply) => {
      let keys = reply;

      keys.forEach((key, i) => {
        redis.get(key, (err, reply) => {
          latest[key] = reply;
          if (i === keys.length - 1) {
            resolve(latest);
          }
        });
      });
    });
  });
}

function setEntryPrice(assetPair, entryPrice, entryPriceExpiry) {
  return new Promise((resolve, reject) => {
    redis.set("buy" + assetPair + "At", entryPrice, "EX", entryPriceExpiry, (err, reply) => {
      if (!err) {
        resolve(reply);
      }
      reject();
    });
  });
}

module.exports = {
  init,
  setToCache: setToRedis,
  getFromCache: getFromRedis,
  delFromCache: delFromRedis,
  getLatestKeyValuePairs,
  setEntryPrice,
};